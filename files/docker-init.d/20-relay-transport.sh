#!/usr/bin/env bash

set -Eeuo pipefail

if [[ -z "$POSTFIX_RELAY_TRANSPORT" ]]; then
  exit 0
fi

if ! [[ "$POSTFIX_RELAY_TRANSPORT" =~ ^\[.*\]:[[:digit:]]+$ ]]; then
  echo "The relay_transport address does not use the square brackets notation [server]:port." \
    "That results in MX lookups on the server address. Are you sure you want that?"
fi

update-postfix-config.sh <(echo "relay_transport = relay:$POSTFIX_RELAY_TRANSPORT")

# enable XFORWARD
postconf -P "relay/unix/smtp_send_xforward_command=yes"
# make the TLS encryption mandatory
postconf -P "relay/unix/smtp_tls_security_level=encrypt"
