#!/usr/bin/env bash

set -Eeuo pipefail

if [[ -z "$POSTFIX_SMTPD_MILTERS" ]]; then
  exit 0
fi

for POSTFIX_SMTPD_MILTER in $POSTFIX_SMTPD_MILTERS; do
  if ! [[ "$POSTFIX_SMTPD_MILTER" =~ ^\[.*\]:[[:digit:]]+$ ]]; then
    echo "One of the SMTPD milter addresses does not use the square brackets notation [server]:port." \
      "That results in MX lookups on the server address. Are you sure you want that?"
  fi

  update-postfix-config.sh <(echo "smtpd_milters = $(postconf -h smtpd_milters) inet:$POSTFIX_SMTPD_MILTER")
done
