#!/usr/bin/env bash

set -Eeuo pipefail

(ls /opt/postfix/conf.d/*.cf || true) 2>/dev/null | xargs /usr/local/sbin/update-postfix-config.sh